/*
Project : LaserGun V 0.1 
Page: http://jungletronics.blogspot.com.br/2014/06/lasergun-project.html
YouTube: http://youtu.be/andpnW46vQE
Code: https://bitbucket.org/giljr/lasergunproject
File : CoM_test_arduino_pan_tilt

this aims initial tests the operation of the laser in Processing 
with Arduino communicating via Firmata library; In Arduino load 
Examples > Firmata > StandardFirmata.ino; Please import on Processing 
Sketch > Import Library... > Add Library > Arduino (Firmata)

P.Velho - Brazil
giljr.2009@gmail.com (Gil Jr)
Date: 29/06/2014
*/

import SimpleOpenNI.*;
import processing.serial.*;
import cc.arduino.*;

SimpleOpenNI kinect;

Arduino arduino;

int servoPin9 = 9;
int servoPin10 = 10; // Control pin for servo motor


void setup() {
  size(640, 480);
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth();
  kinect.enableRGB();
  //kinect.enableUser(SimpleOpenNI.SKEL_PROFILE_NONE); 
  kinect.enableUser(); 
  //1

  arduino = new Arduino(this, Arduino.list()[0]);
  arduino.pinMode(servoPin9, Arduino.OUTPUT);
  arduino.pinMode(servoPin10, Arduino.OUTPUT);
  // note - we are setting a digital pin to output
}
void draw() {
  kinect.update();
  //image(kinect.depthImage(), 0, 0);
  image(kinect.rgbImage(), 0, 0);
  IntVector userList = new IntVector();
  kinect.getUsers(userList);
  for (int i=0; i<userList.size(); i++) { 
    //2
    int userId = userList.get(i);
    PVector position = new PVector();
    kinect.getCoM(userId, position); 
    //3
    kinect.convertRealWorldToProjective(position, position);
    fill(255, 0, 0);
    ellipse(position.x, position.y, 25, 25);

    textSize(40);
    //text(userId, position.x, position.y);
    /*
    Range: ~ 50 cm to 5 m. Can get closer (~ 40 cm) in parts, but can't have the full view be < 50 cm.
    Horizontal Resolution: 640 x 480 and 45 degrees vertical FOV and 58 degrees horizontal FOV. Simple geometry shows is about ~ 0.75 mm per pixel x by y at 50 cm, and ~ 3 mm per pixel x by y at 2 m.
    Depth resolution: ~ 1.5 mm at 50 cm. About 5 cm at 5 m.
    Noise: About +-1 DN at all depths, but DN to depth is non-linear. This means +-1 mm close, and +- 5 cm far.

    */
    int xMapped = (int)map(position.x,0,640, 45, 135);
    //xMapped = constrain(xMapped,0,80);
    int yMapped = (int)map(position.y,0,480, 48,98);
        
    print(xMapped);
    print(" - ");
    println(yMapped);
    
    arduino.analogWrite(servoPin10, (int)xMapped); // the servo moves to the horizontal location of the mouse
    arduino.analogWrite(servoPin9, (int)yMapped); // the servo moves to the horizontal location of the mouse
  }
}
