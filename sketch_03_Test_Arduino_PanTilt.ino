/*
Project : LaserGun V 0.1 
Page: http://jungletronics.blogspot.com.br/2014/06/lasergun-project.html
YouTube: http://youtu.be/andpnW46vQE
Code: https://bitbucket.org/giljr/lasergunproject
File : sketch_03_Test_Arduino_PanTilt

this aims initial tests the operation of the laser in Processing 
with Arduino communicating via Firmata library; In Arduino load 
Examples > Firmata > StandardFirmata.ino; Please import on Processing 
Sketch > Import Library... > Add Library > Arduino (Firmata)

P.Velho - Brazil
giljr.2009@gmail.com (Gil Jr)
Date: 29/06/2014
*/

#include  <Servo.h>                                       //include la libreria Servo.h necessaria per questo programma

Servo Pan;                                                     //Dichiara i due servi Pan e Tilt
Servo Tilt;


int   potP = 2;                                               //pin lettura potenziometri
int   potT = 3;
int   valP;                                                      //variabili per lettura potenziometri
int   valT;

void setup() {
  Serial.begin(9600);
  Pan.attach(9);                                          //assegna pin 9 e 10 come output del segnale per controllare i servi
  Tilt.attach(10);
  int   potP = 2;                                               //pin lettura potenziometri
  int   potT = 3;



}

void loop() {
  valP = analogRead(potP);     //legge il valore di potP e lo assegna a valP. Valore compreso tra 0 e 1023
  valT = analogRead(potT);

  valP = map(valP, 0, 1023, 0, 180);             //Aggiorna valP con valore relativo in scala compresa tra 0 e 180
  valT = map(valT, 0, 1023, 0, 180);
  //  Serial.print("P=");
  //  Serial.print(valP);
  //  Serial.print("T=");
  //  Serial.println(valT);
  //  delay(1000);
  Pan.write(valP);
  delay(100);
  Tilt.write(valT);
  delay(100);
  // Pan.write(180);
  // delay(1000);
  // delay(1000);
  // Tilt.write(0);
  // delay(1000);
  // Tilt.write(90);
  // delay(1000);
  // Tilt.write(180);
  // delay(1000);

}



