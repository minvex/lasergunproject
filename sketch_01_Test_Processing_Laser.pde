/*
Project : LaserGun V 0.1 
Page: http://jungletronics.blogspot.com.br/2014/06/lasergun-project.html
File : sketch_01_Test_Processing_Laser

this aims initial tests the operation of the laser in Processing 
with Arduino communicating via Firmata library; In Arduino load 
Examples > Firmata > StandardFirmata.ino; Please import on Processing 
Sketch > Import Library... > Add Library > Arduino (Firmata)

P.Velho - Brazil
giljr.2009@gmail.com (Gil Jr)
Date: 29/06/2014
*/

import processing.serial.*;
import cc.arduino.*;

Arduino arduino;
int ledPin = 13;

void setup()
{
  //println(Arduino.list());
  arduino = new Arduino(this, Arduino.list()[0], 57600);
  println(arduino.list()[0]);
  arduino.pinMode(ledPin, Arduino.OUTPUT);
}

void draw()
{
  arduino.digitalWrite(ledPin, Arduino.HIGH);
  delay(1000);
  arduino.digitalWrite(ledPin, Arduino.LOW);
  delay(1000);
}
